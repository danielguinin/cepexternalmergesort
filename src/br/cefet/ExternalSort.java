package br.cefet;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class ExternalSort {

	public static void externalSort(Scanner scanner, Scanner scanner2, int qtdLinhasBloco) throws IOException{
		String tfile = "temp-file-";
		int qtdLinhas = LeituraEscrita.contadorDeLinhas(scanner2);
		
		try {
		int qtdArquivos = 0;
		
		//Dividindo o arquivo em blocos, ordenando e armazenando em arquivos separados
		while(scanner.hasNext()) {
    		ArrayList<Endereco> bloco = LeituraEscrita.leitor(scanner, qtdLinhasBloco);
        	Collections.sort(bloco);
        	String textoPronto = LeituraEscrita.quebraLinhas(bloco);
        	LeituraEscrita.escritor(tfile + Integer.toString(qtdArquivos) + ".txt", textoPronto);
        	qtdArquivos++;
		}
		
		ArrayList<Endereco> topCeps = new ArrayList<Endereco>();
		BufferedReader[] blocoDeEnderecos = new BufferedReader[qtdArquivos];

		//lendo a primeira linha de cada bloco e escrevendo esta em um ArrayList
		for (int i = 0; i < qtdArquivos; i++) {
			blocoDeEnderecos[i] = new BufferedReader(new FileReader(tfile + Integer.toString(i) + ".txt"));
			String primeiroCep = blocoDeEnderecos[i].readLine();
			if (primeiroCep != null){
				topCeps.add(i, LeituraEscrita.leitorDeRegistros(primeiroCep));
			}
			else{
				Endereco end = new Endereco("a", "a", "a", "a", "a", "99999999");
				topCeps.add(i, end);
			}
		}
		
		FileWriter fw = new FileWriter("C:\\test\\cep-ordenado.txt");
		PrintWriter pw = new PrintWriter(fw);
		
		// Abrindo cada arquivo e fazendo o merge para um arquivo final
		for (int i = 0; i < qtdLinhas; i++) {
			Endereco menorCep = null;
			long min = topCeps.get(0).getNumCep();
			int posicaoMenorCep = 0;
			
			for (int j = 0; j < qtdArquivos; j++) {
				if (min > topCeps.get(j).getNumCep()) {
					min = topCeps.get(j).getNumCep();
					menorCep = topCeps.get(j);
					posicaoMenorCep = j;
				}
			}
			
			//escreve o endere�o com menor cep no arquivo final
			pw.println(menorCep);
			
			//pega a pr�xima linha do buffer que estava o endere�o de menor cep
			String proximoEndereco = blocoDeEnderecos[posicaoMenorCep].readLine();
			if (proximoEndereco != null)
				topCeps.add(posicaoMenorCep, LeituraEscrita.leitorDeRegistros(proximoEndereco));
			else{
				Endereco end = new Endereco("a", "a", "a", "a", "a", "99999999");
				topCeps.add(posicaoMenorCep, end);
			}
		}
		
		for (int i = 0; i < qtdArquivos; i++)
			blocoDeEnderecos[i].close();

		pw.close();
		fw.close();
		
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public static void main(String args[]) throws IOException {
    	
    	String path = new ExternalSort().getClass().getResource("Cep.dat").getPath();

    	System.out.println("Digite o tamanho do bloco:");
    	Scanner scan = new Scanner(System.in);
    	int quantidadeRegistros = scan.nextInt();
    	
    	Scanner scanner = new Scanner(new FileReader(path)).useDelimiter("\\n");
    	Scanner scanner2 = new Scanner(new FileReader(path)).useDelimiter("\\n");
    	ExternalSort.externalSort(scanner, scanner2, quantidadeRegistros);
    	
    }
}
